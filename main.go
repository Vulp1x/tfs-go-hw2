package main

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
)

type Edge struct {
	from Node
	to   Node
}

type Node struct {
	Email     string
	CreatedAt string
}

type outputJSON struct {
	ID   int    `json:"id"`
	From string `json:"from"`
	To   string `json:"to"`
	Path []Node `json:"path,omitempty"`
}

type Graph struct {
	nodes map[string]Node
	edges map[string][]Edge
}

type NodeList struct {
	Nick        string     `json:"Nick"`
	Email       string     `json:"Email"`
	CreatedAt   string     `json:"Created_at"`
	Subscribers []NodeList `json:"Subscribers"`
}

func (n *NodeList) toNode() Node {
	if n != nil {
		return Node{
			Email:     n.Email,
			CreatedAt: n.CreatedAt,
		}
	}

	return Node{}
}

func main() {
	usersFileFlag := flag.String("usersFilePath", "users.json", "Path to JSON with users")
	outputFileFlag := flag.String("outputFilePath", "output.json", "Path to output file")
	inputFileFlag := flag.String("inputFilePath", "input.csv", "Path to file with pairs of users")
	flag.Parse()

	nodes, err := parseUsers(*usersFileFlag)
	if err != nil {
		log.Fatal("failed to parse input file: ", err)
	}

	g, err := buildGraph(nodes)
	if err != nil {
		log.Fatal("failed to build grapg: ", err)
	}

	edges, err := inputParser(*inputFileFlag)
	if err != nil {
		log.Fatal("failed to read input.csv file: ", err)
	}

	b, err := g.generateOutputJSON(edges)
	if err != nil {
		log.Fatal("failed to generate JSON: ", err)
	}

	err = writeToJSON(b, *outputFileFlag)
	if err != nil {
		log.Fatal("failed to write to JSON: ", err)
	}
}

func inputParser(filePath string) ([]Edge, error) {
	f, err := os.Open(filePath)
	if err != nil {
		return nil, fmt.Errorf("failed to open input file: %s", err)
	}
	defer f.Close()

	csvIn := csv.NewReader(f)

	var edges []Edge

	for {
		row, err := csvIn.Read()
		if err != nil {
			if err == io.EOF {
				return edges, nil
			}

			return nil, fmt.Errorf("failed to read a new row: %s", err)
		}

		const (
			sourceEmail = iota
			destinationEmail
		)

		edges = append(edges, Edge{
			from: Node{Email: row[sourceEmail]},
			to:   Node{Email: row[destinationEmail]},
		})
	}
}

func parseUsers(path string) ([]NodeList, error) {
	jsonFile, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("can't open users JSON file: %s", err)
	}
	defer jsonFile.Close()

	b, err := ioutil.ReadAll(jsonFile)

	if err != nil {
		return nil, fmt.Errorf("can't read file: %s", err)
	}

	var node []NodeList

	err = json.Unmarshal(b, &node)
	if err != nil {
		return nil, fmt.Errorf("can't parse json: %s", err)
	}

	return node, nil
}

func buildGraph(nodes []NodeList) (*Graph, error) {
	var graph Graph

	graph.edges = make(map[string][]Edge)
	graph.nodes = make(map[string]Node)

	for _, node := range nodes {
		if _, ok := graph.nodes[node.Email]; ok {
			return nil, fmt.Errorf("this isn't unique email, couldn't build graph: %s", node.Email)
		}

		graph.nodes[node.Email] = node.toNode()

		for _, subscriber := range node.Subscribers {
			graph.edges[node.Email] = append(graph.edges[node.Email], Edge{
				from: node.toNode(),
				to:   subscriber.toNode(),
			})
		}
	}

	return &graph, nil
}

func (g *Graph) breadthFirstSearch(src, dest Node) []Node {
	if src.Email == dest.Email {
		return nil
	}

	var queue []Node

	parents := make(map[Node]Node)
	isVisited := make(map[Node]bool)

	queue = append(queue, g.nodes[dest.Email])

	for len(queue) > 0 {
		node := queue[0]
		queue = queue[1:]

		if node.Email == src.Email {
			return g.createBestPath(node, dest, parents)
		}

		for _, edge := range g.edges[node.Email] {
			if isVisited[edge.to] {
				continue
			}

			isVisited[edge.to] = true
			parents[edge.to] = edge.from

			if edge.to.Email == src.Email {
				return g.createBestPath(edge.to, dest, parents)
			}

			queue = append(queue, edge.to)
		}

		isVisited[node] = true
		g.nodes[node.Email] = node
	}

	return nil
}

func (node *Node) hasParent(parents map[Node]Node) bool {
	_, ok := parents[*node]
	return ok
}

func (g *Graph) createBestPath(child, dest Node, parents map[Node]Node) []Node {
	nodes := []Node{child}

	var bestPath []Node

	for len(nodes) > 0 {
		//time.Sleep(time.Second)
		node := nodes[0]
		nodes = nodes[1:]

		if node.hasParent(parents) && node.Email != dest.Email {
			nodes = append(nodes, parents[node])
			bestPath = append(bestPath, parents[node])
		} else {
			return bestPath[:len(bestPath)-1]
		}
	}

	return bestPath[:len(bestPath)-1]
}

func (g *Graph) generateOutputJSON(inputEdges []Edge) ([]byte, error) {
	var out []outputJSON

	const IDStartsWith = 1

	for i, edge := range inputEdges {
		out = append(out, outputJSON{
			ID:   i + IDStartsWith,
			From: edge.from.Email,
			To:   edge.to.Email,
			Path: g.breadthFirstSearch(edge.from, edge.to),
		})
	}

	tempBytes, err := json.Marshal(out)
	if err != nil {
		return nil, fmt.Errorf("failed to write to json: %s", err)
	}

	return tempBytes, nil
}

func writeToJSON(b []byte, filePath string) error {
	f, err := os.Create(filePath)
	if err != nil {
		return fmt.Errorf("failed to create output file: %s", err)
	}
	defer f.Close()

	var out bytes.Buffer

	err = json.Indent(&out, b, "", "\t")
	if err != nil {
		return fmt.Errorf("failed to format JSON: %s", err)
	}

	_, err = out.WriteTo(f)
	if err != nil {
		return fmt.Errorf("failed to write to output file: %s", err)
	}

	return nil
}
